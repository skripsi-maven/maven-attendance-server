'use strict';
module.exports = (sequelize, DataTypes) => {
  var attendance = sequelize.define('attendance', {
    userid:{
      type: DataTypes.INTEGER,
      allowNull: false
    }, 
    date:{
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    time:{
      type: DataTypes.TIME,
      allowNull: false
    },
    photo:{
      type: DataTypes.TEXT,
      allowNull: true
    },
    status:{
      type: DataTypes.INTEGER,
      allowNull: true
    },
    absence_type:{
      type: DataTypes.STRING,
      allowNull: true
    },
    longitude:{
      type: DataTypes.DECIMAL(9,6),
      allowNull: true
    },
    latitude:{
      type: DataTypes.DECIMAL(9,6),
      allowNull: true
    },
    note:{
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {});
  attendance.associate = function(models) {
    // associations can be defined here
  };
  return attendance;
};