'use strict';
module.exports = (sequelize, DataTypes) => {
  var employee = sequelize.define('employee', {
    fullname:{
      type: DataTypes.STRING,
      allowNull: false
    }, 
    email:{
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password:{
      type: DataTypes.STRING,
      allowNull: false,
    },
    profile_picture:{
      type: DataTypes.STRING,
      allowNull: true
    },
    role:{
      type: DataTypes.STRING,
      allowNull: false
    },
    annual_leave:{
      type: DataTypes.INTEGER,
      allowNull: true
    },
    total_absent:{
      type: DataTypes.INTEGER,
      allowNull: true
    },  
    salt:{
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});
  employee.associate = function(models) {
    // associations can be defined here
  };
  return employee;
};