'use strict';
module.exports = (sequelize, DataTypes) => {
  var summary = sequelize.define('summary', {
    userid:{
      type: DataTypes.INTEGER,
      allowNull: false
    }, 
    month:{
      type: DataTypes.STRING,
      allowNull: false
    },
    year:{
      type: DataTypes.INTEGER,
      allowNull: false
    },
    total_attendance:{
      type: DataTypes.INTEGER,
      allowNull: false
    },
    total_absent:{
      type: DataTypes.INTEGER,
      allowNull: false
    },
    cumulative_hour:{
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {});
  summary.associate = function(models) {
    // associations can be defined here
  };
  return summary;
};