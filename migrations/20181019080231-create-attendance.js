'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('attendances', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userid:{
        type: Sequelize.INTEGER,
        allowNull: false
      }, 
      date:{
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      time:{
        type: Sequelize.TIME,
        allowNull: false
      },
      photo:{
        type: Sequelize.TEXT,
        allowNull: true
      },
      status:{
        type: Sequelize.INTEGER,
        allowNull: true
      },
      absence_type:{
        type: Sequelize.STRING,
        allowNull: true
      },
      longitude:{
        type: Sequelize.DECIMAL(9,6),
        allowNull: true
      },
      latitude:{
        type: Sequelize.DECIMAL(9,6),
        allowNull: true
      },
      note:{
        type: Sequelize.STRING,
        allowNull: true
      }
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('attendances');
  }
};