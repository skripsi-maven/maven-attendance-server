'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('employees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fullname:{
        type: Sequelize.STRING,
        allowNull: false
      }, 
      email:{
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      password:{
        type: Sequelize.STRING,
        allowNull: false
      },
      profile_picture:{
        type: Sequelize.STRING,
        allowNull: true
      },
      role:{
        type: Sequelize.STRING,
        allowNull: false
      },
      annual_leave:{
        type: Sequelize.INTEGER,
        allowNull: true
      },
      total_absence:{
        type: Sequelize.INTEGER,
        allowNull: true
      }, 
      salt:{
        type: Sequelize.STRING,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('employees');
  }
};