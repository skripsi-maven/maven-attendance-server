'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('summaries', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userid:{
        type: Sequelize.INTEGER,
        allowNull: false
      }, 
      month:{
        type: Sequelize.STRING,
        allowNull: false
      },
      year:{
        type: Sequelize.INTEGER,
        allowNull: false
      },
      total_attendance:{
        type: Sequelize.INTEGER,
        allowNull: false
      },
      total_absent:{
        type: Sequelize.INTEGER,
        allowNull: false
      },
      cumulative_hour:{
        type: Sequelize.INTEGER,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('summaries');
  }
};