var express = require('express');
var router = express.Router();
var model = require('../models/index');
var multer = require('multer');
var schedule = require('node-schedule');

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'photo/')
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now()+'.jpg')
    }
});

var upload = multer({storage: storage});

// router.post('/foto', function(req, res, next) {
// 	upload.single('photo')
router.post('/foto', upload.single('photo'), async (req, res) => {
    try {
        const col = await loadCollection(COLLECTION_NAME, db);
        const data = col.insert(req.file);

        db.saveDatabase();
        res.status(200).json({
			meta:{
				message: 'You have absent in!',
				code: 200
			}
		})
        // res.send({ id: data.$loki, fileName: data.filename, originalName: data.originalname });
    } catch (err) {
        res.sendStatus(400);
    }
});

router.post('/in',upload.single('photo'), async(req, res, next)=> {
	try {
        const col = await loadCollection(COLLECTION_NAME, db);
        const data = col.insert(req.file);

        db.saveDatabase();

        res.send({ id: data.$loki, fileName: data.filename, originalName: data.originalname });
    } catch (err) {
        // res.sendStatus(400);
        res.status(200).json({
			meta:{
				message: 'You have absent in!',
				code: 200
			}
		})
    }

	var photo = 'photo' + '-' + Date.now()+'.jpg';

	const{
		userid,
		date,
		status,
		longitude,
		latitude,
		note
	} = req.body;

	model.attendance.findOne({
  	  	where: {
	        userid: userid,
	        date: date
	    }
  	}).then(attendance=>{
	 //    if(attendance)
		// {
		// 	if(attendance.status!=null)
		// 	{
		// 		res.status(200).json({
		// 			meta:{
		// 				message: 'You cant absent in again today'
		// 			}
		// 		});
		// 	}
		// }
		// else
		// {
			model.attendance.create({
				userid: userid,
				date: date,
				photo: photo,
				status: status,
				longitude: longitude,
				latitude: latitude,
				note: note
			})
			.then(categories=>{
				 res.status(200).json({
					meta:{
						message: 'You have absent in!',
						code: 200
					}
				})
			})
			.catch(error=>res.json({
				error: true,
				error: error
			}));
		// }
	});
});

router.post('/out', function(req, res, next) {
	const{
		userid,
		date,
		photo,
		status,
		longitude,
		latitude,
		note
	} = req.body;

	model.attendance.findOne({
  	  	where: {
	        userid: userid,
	        date: date
	    }
  	})
  	.then(attendance=>{
	    if(attendance)
		{
			if(attendance.status=='in')
			{
				model.attendance.create({
					userid: userid,
					date: date,
					photo: photo,
					status: status,
					longitude: longitude,
					latitude: latitude,
					note: note
				})
				.then(categories=>res.status(200).json({
					meta:{
						message: 'You have absent out!',
						code: 200
					}
				}))
				.catch(error=>res.json({
					error: true,
					error: error
				}));
			}
			else
			{
				res.status(200).json({
					meta:{
						message: 'cant absent out, absent in first'
					}
				});	
			}
		}
		else
		{
			res.status(200).json({
				meta:{
					message: 'You are not absent in yet'
				}
			});
		}
	})
	.catch(error=>res.json({
		error: true,
		error: error
	}));
});

router.post('/attend',upload.single('photo'), async(req, res, next)=> {
	// try {
 //        const col = await loadCollection(COLLECTION_NAME, db);
 //        const data = col.insert(req.file);

 //        db.saveDatabase();

 //        res.send({ id: data.$loki, fileName: data.filename, originalName: data.originalname });
 //    } catch (err) {
 //        // res.sendStatus(400);
 //        res.status(200).json({
	// 		meta:{
	// 			message: 'FUCK YOU ASSHOLES!',
	// 			code: 200
	// 		}
	// 	})
 //    }

	const{
		userid,
		longitude,
		latitude,
		note
	} = req.body;
	
	var date = new Date();
	var hour = date.getHours();
	var minute = date.getMinutes();
	var second = date.getSeconds();
	var time = [hour, minute, second].join(':');
	var dateonly = formatDate(new Date());
	// var photo = 'photo' + '-' + Date.now() + '.jpg';
	// var photo_location = '../photo/' + photo;

	model.attendance.findOne({
  	  	where: {
	        userid: userid,
	        date: date
	    }
  	})
  	.then(attendance=>{
	    if(attendance)
		{
			if(attendance.absence_type=='In')
			{
				attendance.updateAttributes({
					absence_type: "In, already out"
				})
				var absence_type = "Out";
				var dateonly = formatDate(new Date());
				var photo = 'photo-' + userid + '-' + dateonly + '-out' +'.jpg';
				var status = 0;
				model.attendance.create({
					userid: userid,
					date: dateonly,
					time: time,
					photo: photo,
					status: status,
					absence_type: absence_type,
					longitude: longitude,
					latitude: latitude,
					note: note
				})
				.then(categories=>res.status(200).json({
					meta:{
						message: 'Fuck You',
						code: 200
					}
				}))
				.catch(error=>res.json({
					error: true,
					error: error
				}));
			}
			else
			{
				res.status(400).json({
					meta:{
						message: 'You cant absent in again today'
					}
				});	
			}
		}
		else
		{	
			var absence_type = "In";
			var dateonly = formatDate(new Date());
			var photo = 'photo-' + userid + '-' + dateonly + '-in' +'.jpg';
			var status = 0;
			model.attendance.create({
				userid: userid,
				date: dateonly,
				photo: photo,
				time: time,
				status: status,
				absence_type: absence_type,
				longitude: longitude,
				latitude: latitude,
				note: note
			})
			.then(categories=>res.status(200).json({
				meta:{
					message: 'You have absent in!',
					code: 200
				}
			}))
			.catch(error=>res.json({
				error: true,
				error: error
			}));
		}
	})
	.catch(error=> {
		res.status(500).json({
			error: true,
			data: [],
			error: error
		})
	})
});

var auto_absent = schedule.scheduleJob('59 23 * * 1,2,3,4,5', function(){
  	console.log('The answer to life, the universe, and everything!');

});

router.get('/timesheet/:id/:date', function(req, res, next) {

	model.attendance.findAll({
		attributes: ['time', 'photo', 'status', 'absence_type', 'note'],
  		where: {
	        userid: req.params.id,
	        date: req.params.date
	    }
  	})
  	.then(attendance=>{
		if(attendance.length>1)
		{
			res.status(200).json({
				attendance
			})
		}
		else
		{
			res.status(200).json({
				attendance
			})
			// res.status(200).json({
			// 	meta:{
			// 		message: 'User not found or user not absent in yet'
			// 	}
			// });
		}
	})
	.catch(error=> {
		res.status(500).json({
			error: true,
			data: [],
			error: error
		})
	})
});

router.get('/status/:id', function(req, res, next) {

	model.attendance.findOne({
		attributes: ['userid', 'status'],
  		where: {
	        userid: req.params.id,
	        date: req.body.date
	    }
  	})
	.then(attendance=>{
		if(attendance)
		{
			res.status(200).json({
				attendance
			})
		}
		else
		{
			res.status(200).json({
				meta:{
					message: 'User not found or user not absent in yet'
				}
			});
		}
	})
	.catch(error=> {
		res.status(500).json({
			error: true,
			data: [],
			error: error
		})
	})
});

router.get('/statistic/:id', function(req, res, next) {

	model.attendance.findOne({
  		where: {
	        userid: req.params.id,
	        date: req.body.date
	    }
  	})
	.then(attendance=>{
		if(attendance)
		{
			res.status(200).json({
				attendance
			})
		}
		else
		{
			res.status(200).json({
				meta:{
					message: 'User not found or user not absent in yet'
				}
			});
		}
	})
	.catch(error=> {
		res.status(500).json({
			error: true,
			data: [],
			error: error
		})
	})
});

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

router.post('/leave', function(req, res, next) {
	const{
		userid,
		absence_type,
		note
	} = req.body;
	
	var time = '00:00:00';
	var dateonly = formatDate(new Date());
	var photo = '';
	var longitude = '0,0';
	var latitude = '0,0';
		
	model.attendance.findOne({
  	  	where: {
	        userid: userid,
	        date: dateonly
	    }
  	})
  	.then(attendance=>{
	    if(attendance)
		{
			if(attendance.absence_type=='In'||attendance.absence_type=='Out'||attendance.absence_type=='In, already out')
			{
				res.status(200).json({
					meta:{
						message: 'Anda sudah melakukan absen, tidak bisa meminta cuti'
					}
				});	
			}
			else
			{
				res.status(400).json({
					meta:{
						message: 'Anda sudah meminta cuti hari ini'
					}
				});	
			}
		}
		else
		{	
			var status = 1;
			model.attendance.create({
				userid: userid,
				date: dateonly,
				photo: photo,
				time: time,
				status: status,
				absence_type: absence_type,
				longitude: longitude,
				latitude: latitude,
				note: note
			})
			.then(attendance=>{

				model.employee.findOne({
			  	  	where: {
				        id: userid,
				    }
			  	})
			  	.then(employee=>{
			  		var annual_leave = employee.annual_leave;
			  		var remaining_annual_leave = annual_leave+1;
			  		employee.updateAttributes({
						annual_leave: remaining_annual_leave
					})

					res.status(200).json({
						meta:{
							message: 'Cuti anda sudah diajukan'
						}
					});
			  	})
			  	.catch(error=>res.json({
					error: true,
					error: error
				}));	
			})
			.catch(error=>res.json({
				error: true,
				error: error
			}));
		}
	})
	.catch(error=> {
		res.status(500).json({
			error: true,
			data: [],
			error: error
		})
	})
});

module.exports = router;