var express = require('express');
var router = express.Router();
var model = require('../models/index');
var Pushy = require('pushy');
 
var pushyAPI = new Pushy('9ced168f0ba5e48ae37ef24c0d0acff3e288f61e6b3628faf77c4b1bf7e93576');
 
var data = {
    "userId":23,
	"title":"Notif Title",
	"description":"Absen Woi",
	"click_action":"Absen Woi",
	"ref_id":88
};
 
var to = ['925052452f5e7f80dd2521'];

var options = {
    notification: {
        badge: 1,
        sound: 'ping.aiff',
        body: 'Hello World \u270c'
    },
};

router.post('/sendnotif', function(req, res, next) {
	pushyAPI.sendPushNotification(data, to, options, function (err, id) {
	    // Log errors to console 
	    if (err) {
	        return console.log('Fatal Error', err);
	    }
	    else
	    {
	    	res.status(200).json({
				meta:{
					message: 'Notif has been sent!',
					code: 200
				}
			})
	    }
	    // Log success 
	    console.log('Push sent successfully! (ID: ' + id + ')');
	});
});

module.exports = router;