var express = require('express');
var router = express.Router();
var model = require('../models/index');
var nodemailer = require('nodemailer');

var generate = function(limit){
	var limit = limit;
	var salt = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	for (var i = 0; i < limit; i++)
  	{
    	salt += possible.charAt(Math.floor(Math.random() * possible.length));
  	}
  	return salt;
}

//create new user 	done
//Login           	done
//delete user 		done
//edit profile (patch) done
//get all user  

router.post('/create', function(req, res, next) {
	var md5 = require('md5');
	const{
		fullname,
		email,
		password,
		profile_picture,
		role
	} = req.body;

	// if(req.body['role']=="employee")
	// {
	// 	res.status(201).json({
	// 	meta:{
	// 		message: 'You dont have permission to do this!!',
	// 		code: 200
	// 		}
	// 	})	
	// }
	// else
	// {
		var salt = generate(5);
	  	var hashPassword = md5(password+salt);
		var token = generate(25);

		model.employee.create({
			fullname: fullname,
			email: email,
			password: hashPassword,
			profile_picture:profile_picture,
			role:role,
			salt: salt,
			token: token
		})
		.then(employee=>res.status(201).json({
			meta:{
				message: 'New employee has been created!!',
				code: 200
			},
			data:{
				fullname: req.body.fullname,
				token: employee.token
			}
		}))
		.catch(error=> {
			res.status(500).json({
				error: true,
				data: [],
				error: error
			})
		})
	// }
});

router.post('/login', function(req, res, next) {
	var md5 = require('md5');
  	var email = req.body.email;
  	var password = req.body.password;
  	var device_token = req.body.device_token;

	model.employee.findOne({
  	  	where: {
	        email: email
	    }
  	}).then(employee=>{
    if(employee)
	{	
		var salt= employee.salt;
  	  	var hashPassword = md5(password+salt);
  	
  	  	if(employee.password==hashPassword)
  	  	{
	  		var token = generate(25);

			employee.updateAttributes({
				token: token,
				device_token: device_token
			});

  	  		res.json({
				meta:{
					message: 'Login Success!!',
					code: 200
				},
				data:{
					id: employee.id,
					fullname: employee.fullname,
					device_token: device_token	
					// token: employee.token
				}
			});
  	  	}
  	  	else
  	  	{
  	  		res.status(401).json({
				meta:{
					message: 'wrong password!',
					code: 404
				}
			});
  		}
  	}
  	else
  	{
  		res.status(404).json({
			meta:{
				message: 'employee not found!!.',
				code: 404
			}
		});
  	}
  	})
  	.catch(error => {
  		res.json({
		message: 'failed',
		error: error
	  	})	
  	});
});

router.get('/all', function(req, res, next){
	model.employee.findAll({
		attributes: ['id', 'fullname', 'email', 'profile_picture', 'role', 'annual_leave', 'total_absent']
	})
  	.then(employee=>{
  		res.status(200).json({
			employee 
		})
  	})
  	.catch(error=>{
  		res.json({
			error: true,
			error: error
		})
  	})
});

router.get('/:id', function(req, res, next){
	model.employee.findOne({
		attributes: ['fullname', 'email', 'profile_picture', 'role', 'annual_leave', 'total_absent'],
  		where: {
	        id: req.params.id
	    }
  	})
  	.then(employee=>{
  		res.status(200).json({
			employee 
		})
  	})
  	.catch(error=>{
  		res.json({
			error: true,
			error: error
		})
  	})	
});

router.patch('/:id', function(req, res, next){
	const{
		fullname,
		// profile_picture,
	} = req.body;

	model.employee.findOne({
  		where: {
	        id: req.params.id
	    }
  	})
  	.then(employee=>{
  		if(employee)
  		{
  			employee.updateAttributes({
				fullname: fullname,
				// profile_picture:profile_picture,
			})
			res.status(201).json({
				meta:{
					message: 'Employee data has been updated!!',
					code: 200
				},
				data:{
					fullname: req.body.fullname,
					token: employee.token
				}
			})
  		}
  		else
	  	{
	  		res.status(404).json({
				meta:{
					message: 'employee not found!!.',
					code: 404
				}
			});
	  	}
  	})
	.catch(error=> {
		res.status(500).json({
			error: true,
			data: [],
			error: error
		})
	})
});

router.delete('/:id', function(req, res, next){
	
	model.employee.findOne({
  		where: {
	        id: req.params.id
	    }
  	})
  	.then(employee=>{
  		if(employee)
  		{
		    model.employee.destroy({ where: {
		        id: id
		    }})
		    .then(status => res.status(200).json({
			   	meta:{
					message: 'employee has been deleted!!',
					code: 200
				}
		    }))
	    	.catch(error => res.json({
	    		message: 'id didnt exist',
	        	error: true,
	        	error: error
	        }));
		}
	  	else
	  	{
	  		res.status(401).json({
				meta:{
					message: 'employee not found!!',
					code: 401
				}
			});
	  	}
  	})
  	.catch(error=>{
  		res.json({
			error: true,
			error: error
		})
  	})
});

router.post('/change/:id', function(req, res, next) {
	
	model.employee.findOne({
  		where: {
	        id: req.params.id
	    }
  	})
  	.then(employee=>{
		var md5 = require('md5');

		var oldPassword = req.body.oldPassword;
		var newPassword = req.body.newPassword;
		var salt = employee.salt;
	  	var hashPassword = md5(oldPassword+salt);

	  	if(employee.password==hashPassword)
	  	{
	  		var salt2 = generate(5);
			var newHashPassword = md5(newPassword + salt2);

			employee.updateAttributes({
				password : newHashPassword,
				salt: salt2
			});
			
	  		res.status(200).json({
				meta:{
					message: 'Change Password Success!!'
				}
			});
	  	}
	  	else
	  	{
	  		res.status(404).json({
				meta:{
					message: 'Password didnt match!'
				}
			});
		}
	})
  	.catch(error=>{
		res.status(404).json({
			meta:{
				message: 'User not found!!'
			}
		});
  	})	
});

var transporter = nodemailer.createTransport({
	// host: 'smtp.mail.yahoo.com',
	port: 465,
	service: 'gmail',
	secure: false,
	auth: {
	   // user: 'glenn_vincen@yahoo.com',
	   // pass: 'StormRage@888'
	   user: 'attendance@maven.co.id',
	   pass: 'mavenmaven'
	},
	debug: false,
	logger: true
});

router.post('/forget', function(req, res, next) {

	var email = req.body.email;
	var md5 = require('md5');

	model.employee.findOne({
  		where: {
	        email: req.body.email
	    }
  	})
  	.then(employee=>{
		var salt = generate(5);
		var newPassword = generate(8);
		var newHashPassword = md5(newPassword+salt);

		employee.updateAttributes({
			password : newHashPassword,
			salt: salt
		});

		const mailOptions = {
			//from: 'glenn_vincen@yahoo.com', // sender address
			from: 'attendance@maven.co.id',
			to: email, 						// list of receivers
			subject: 'Your new password for Maven Attendance',		// Subject line
			html: 'Your new password : ' + newPassword + '<br>' + 'You can change your password later in Profile page'			// plain text body
		};
		transporter.sendMail(mailOptions, function (err, info) {
			if(err)
			  console.log(err)
			else
			  console.log(info);
		});

		res.status(200).json({
			meta:{
				message: 'Password successfully changed, Please check your email in a few moment.'
			}
		});
	})
  	.catch(error=>{
		res.status(404).json({
			meta:{
				message: 'Enter valid email address!!'
			}
		});
  	})	
});	

module.exports = router;