var express = require('express');
var router = express.Router();
var check = require('validator').check,
    sanitize = require('validator').sanitize;


/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', { title: 'Express' });
  
});

module.exports = router;
