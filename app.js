const express = require('express');

const logger = require('morgan');

const bodyParser = require('body-parser');

// This will be our application entry. We'll setup our server here.

const http = require('http');
const app = express();

var index = require('./routes/index');
var attendance = require('./routes/attendance');
var employee = require('./routes/employee');
var summary = require('./routes/summary');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', index);
app.use('/attendance', attendance);
app.use('/employee', employee);
app.use('/summary', summary);
app.use('/photo', express.static('photo'));

app.get('*', (req, res) => res.status(200).send({
	message: '-',
}));

const port = parseInt(process.env.PORT, 10) || 8000;

app.set('port', port);

const server = http.createServer(app);

server.listen(port);

module.exports = app;

